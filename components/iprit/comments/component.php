<?php
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) {
    die();
}

/**
 * Указываем необходимые пространства имен
 */
use Bitrix\IpritTools;
use Bitrix\IpritTools\Exceptions;
use Bitrix\IpritComments;
use Bitrix\Main\Engine\CurrentUser;

/**
 * Автозагрузка своих классов, которые используются в компоненте
 */
spl_autoload_register(function($class) {
    if(substr_count($class, 'Bitrix\IpritTools') > 0 || substr_count($class, 'Bitrix\IpritComments') > 0) {
        $path = __DIR__ . '/classes/' . str_replace(array('Bitrix\\', '\\', 'Table'), array('', '/', ''), $class) . '.class.php';
    
        if(!file_exists($path)) {
            throw new Exceptions\NoClassFileException($path);
        }

        require_once $path;
    }
});

/**
 * Получаем информацию о текущем пользователе
 */
$currentUser = CurrentUser::get();

/**
 * Получаем данные из массива $_POST
 * Если пришел комментарий, пытаемся его сохранить
 */
$values = filter_input_array(INPUT_POST);

if($values['ajax_add_comment']) {
    /**
     * Список обязательных полей
     */
    $required_fields = array('name', 'text');
    $error = [];
    
    /**
     * Если пользователь авторизован, то берем его ФИО
     */
    if($currentUser -> getId() > 0) {
        $values['name'] = $currentUser -> getFullName();
    }
    
    /**
     * Проверяем обязательные поля
     */
    foreach($values as $key => $value) {
        if($value == '' && in_array($key, $required_fields)) {
            $error[$key] = true;
        }
    }
    
    /**
     * Если обязательные поля заполнены, сохраняем комментарий
     */
    if(count($error) == 0) {
        $setComment = new IpritComments\SetComment();
        $setComment ->save($arParams['IBLOCK_ID'], $arParams['CODE'], $values);
    }
    
    /**
     * Отправляем массив с ошибками
     */
    $APPLICATION->RestartBuffer();
    echo json_encode($error);
    die();
}

/**
 * Получаем список комментариев
 */
$commentRes = new IpritComments\GetComments();
$comments = $commentRes->getComments($arParams['IBLOCK_ID'], $arParams['TYPE'], $arParams['CODE']);

$arResult['COMMENTS'] = $comments;

/**
 * Получаем статус пользователя
 */
$arResult['USER_AUTH'] = ($currentUser -> getId() > 0 ? 'Y' : 'N');

/**
 * Подключение компонента
 */
$this->IncludeComponentTemplate();