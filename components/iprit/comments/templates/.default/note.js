$(function ($) {
    'use strict';

    var options = {
            text: 'Alert!',
            theme: 'default',
            timeOut: 3500
        },
        methods = {
            init: function () {
                this.cn = $('<div class=\'snote snote-wrapper \'></div>');
                $('body').append(this.cn);

                window.sNote = this;
            },
            hide: function () {
                var that = this;
                $(this)
                    .fadeOut(1000, function () {
                        that.remove();
                    });
            },

            add: function (option) {
                if (typeof window.sNote === 'undefined') {
                    $.fn.sNote();
                }

                var opt = $.extend({}, options, option),
                    newNote = $('<div class=\'snote__item snote--' + opt.theme + '\'>' + opt.text + '</div>');
                window.sNote.cn.append(newNote);
                setTimeout(methods.hide.bind(newNote), opt.timeOut);
            }

        }

    $.fn.sNote = function (method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist on jQuery.sNote');
        }
    };
}(jQuery));
