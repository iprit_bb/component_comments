<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}

$this->addExternalCss("/bitrix/css/main/bootstrap.css");
$this->addExternalCss($templateFolder . '/note.css');
$this->addExternalJS("https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js");
$this->addExternalJS($templateFolder . '/note.js');

if(is_array($arResult['COMMENTS']) && count($arResult['COMMENTS']) > 0) {
?>
<div class="container">
    <div class="comments">
        <h3 class="title-comments">Комментарии (<?=count($arResult['COMMENTS']);?>)</h3>
        
        <ul class="media-list">
<?php
    $currentDepth = 0;

    foreach($arResult['COMMENTS'] as $key => $comment) {
        
        
        if($comment['DEPTH_LEVEL'] == 0) {
?>
            <li class="media">
                <div class="media-body">
<?php
        } else {
?>
            <div class="media">
                <div class="media-left"></div>
                <div class="media-body">
<?php
        }
?>
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="author"><?=$comment['NAME'];?></div>
                            
                            <div class="metadata">
                                <span class="date"><?=$comment['DATE_CREATE'];?></span>
                            </div>
                        </div>
                        
                        <div class="panel-body">
                            <div class="media-text text-justify"><?=$comment['PREVIEW_TEXT'];?></div>
                        </div>
                        <div class="panel-footer">
                            <a class="btn btn-primary js-comments-answer-btn" data-id="<?=$comment['ID'];?>" href="#">Ответить</a>
                        </div>
                    </div>
<?php
        if($comment['DEPTH_LEVEL'] > $arResult['COMMENTS'][($key + 1)]['DEPTH_LEVEL']) {
            if($arResult['COMMENTS'][($key + 1)]['DEPTH_LEVEL'] == 0) {
                echo str_repeat('</div></div>', ($comment['DEPTH_LEVEL'] - $arResult['COMMENTS'][($key + 1)]['DEPTH_LEVEL']));
?>
                </div>
            </li>
<?php
            } else {
                echo str_repeat('</div></div>', ($comment['DEPTH_LEVEL'] - $arResult['COMMENTS'][($key + 1)]['DEPTH_LEVEL'] + 1));
            }
        } elseif($comment['DEPTH_LEVEL'] == $arResult['COMMENTS'][($key + 1)]['DEPTH_LEVEL']) {
            if($comment['DEPTH_LEVEL'] == 0) {
?>
                </div>
            </li>
<?php
            } else {
?>
                </div>
            </div>
<?php
            }
        }
    }
}
?>
        </ul>
    </div>
</div>

<form class="js-comments-form" action="" method="POST">
    <div class="media">
        <div class="media-body">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="author">Добавить комментарий</div>
                </div>

                <input type="hidden" name="answer" value="" />
                <input type="hidden" name="ajax_add_comment" value="Y" />

                <div class="panel-body">
<?php
if($arResult['USER_AUTH'] != 'Y') {
?>
                    <div class="form-group">
                        <label>Имя:</label>
                        <input type="name" name="name" required class="form-control" placeholder="Введите имя" />
                    </div>
<?php
}
?>
                    <div class="form-group">
                        <label>Комментарий:</label>
                        <textarea class="form-control" placeholder="Введите комментарий" name="text" required ></textarea>
                    </div>
                </div>
                <div class="panel-footer">
                    <button type="submit" class="btn btn-primary">Отправить</button>
                </div>
            </div>
        </div>
    </div>
</form>