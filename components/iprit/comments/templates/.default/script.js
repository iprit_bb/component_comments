window.addEventListener('DOMContentLoaded', function () {
    /**
     * Отправка формы
     */
    $(document).on('submit', 'form.js-comments-form', function(e) {
        e.preventDefault();
        
        var form = $(this);
        var path = window.location.pathname;
        var form_data = new FormData(form[0]);

        //тут должно быть что-то после успешной отправки
        var sendFormOk = function(data) {
            if(Object.keys(data).length > 0) {
                $.fn.sNote('add', {text:'Не заполнены обязательные поля'});
            } else {
                $('.comments form.js-comments-form').remove();
                $('.comments .js-comments-answer-btn').show();
                $.fn.sNote('add', {text:'Ваше сообщение отправлено!'});
            }
        };

        //здесь - если отправить не удалось
        var sendFormError = function() {
            $.fn.sNote('add', {text:'Не удалось отправить сообщение.'});
        };
        
        //отправка данных через ajax
        $.ajax({
            method: 'POST',
            url: path,
            dataType: 'JSON',
            processData: false,
            contentType: false,
            data: form_data,
            success: sendFormOk,
            error: sendFormError
        });
    });
    
    /**
     * Вывод формы при нажатии на кнопку "Ответить"
     */
    $(document).on('click', '.comments .js-comments-answer-btn', function(e) {
        e.preventDefault();
        
        var answerId = $(this).data('id');
        
        $('.comments form.js-comments-form').remove();
        $('.comments .js-comments-answer-btn').show();
        $(this).hide();
        $('form.js-comments-form').clone().appendTo($(this).closest('div'));
        $(this).closest('div').find('form.js-comments-form input[name="answer"]').val(answerId);
    });
});