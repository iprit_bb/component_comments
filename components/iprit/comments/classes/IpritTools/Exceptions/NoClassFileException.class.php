<?php
namespace Bitrix\IpritTools\Exceptions;

class NoClassFileException extends \Exception
{
    public function __construct($file) {
        $this -> message = str_replace('{{message}}', 'Файл ' . $file . ' не найден. Невозможно загрузить класс.', \Bitrix\IpritTools\Tools::getExceptionTemplate());
    }
}
