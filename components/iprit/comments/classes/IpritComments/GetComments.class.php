<?php
namespace Bitrix\IpritComments;

use Bitrix\Main\Loader;
use Bitrix\IpritComments\Tools;
use Bitrix\Iblock\ElementTable;

class GetComments
{
    private $iblockId;
    private $type;
    private $code;
    private $content = [];
    
    /**
     * Подключаем модуль для работы с инфоблоками
     */
    public function __construct() {
        Loader::includeModule('iblock');
    }
    
    /**
     * Получаем отсортированный список комментариев
     * @param int $iblockId - id инфоблока
     * @param type $type - тип: страница или элемент. практического применения никакого не нашел
     * @param type $code - код страницы/id элемента
     * @return array - список комментариев
     */
    public function getComments($iblockId, $type, $code)
    {
        if(Tools::checkIblock($iblockId)) {
            $this -> iblockId = $iblockId;
            $this -> type = $type;
            $this -> code = $code;
        } else {
            return false;
        }
        
        $this -> makeContent();
        $commentList = $this -> sortComments();
        
        return $commentList;
    }
    
    /**
     * Получаем список комментариев из инфоблока
     * @return array
     */
    private function makeContent()
    {
        $content = [];
        
        $contentRes = ElementTable::getList([
            'order' => [
                'DATE_CREATE' => 'ASC'
            ],
            'filter' => [
                'IBLOCK_ID' => $this -> iblockId,
                'CODE' => $this -> code,
                'ACTIVE' => 'Y'
            ],
            'select' => ['ID', 'IBLOCK_ID', 'NAME', 'CODE', 'SORT', 'DATE_CREATE', 'PREVIEW_TEXT', 'ACTIVE'],
            
        ]);
        
        while($element = $contentRes -> fetch()) {
            $element['DATE_CREATE'] = $element['DATE_CREATE'] -> format('d.m.Y H:i');
            $element['NAME'] = htmlspecialcharsEx($element['NAME']);
            $element['PREVIEW_TEXT'] = htmlspecialcharsEx($element['PREVIEW_TEXT']);
            
            $this -> content[$element['SORT']][] = $element;
        }
        
        return $content;
    }
    
    /**
     * Сортируем список комментариев по вложенности. 
     * Указываем уровень вложенности каждого комментария.
     * Метод рекурсивный, параметры нужны для того, чтобы рекурсия нормально работала
     * @param int $parentId - id предка
     * @param int $depthLevel - текущий уровень вложенности
     * @return array
     */
    private function sortComments($parentId = 0, $depthLevel = 0)
    {
        $commentList = [];
        
        foreach($this -> content[$parentId] as $comment) {
            $comment['DEPTH_LEVEL'] = $depthLevel;
            $commentList[] = $comment;
            
            if(is_array($this -> content[$comment['ID']]) && count($this -> content[$comment['ID']]) > 0) {
                $commentList = array_merge($commentList, $this -> sortComments($comment['ID'], ($depthLevel + 1)));
            }
        }
        
        return $commentList;
    }
}
