<?php
namespace Bitrix\IpritComments;

use Bitrix\Main\Loader;

class Tools
{
    public static function checkIblock($iblockId) 
    {
        Loader::includeModule('iblock');
        
        $result = true;
        
        if($iblockId > 0) {
            $iblockData = \CIBlock::GetList(array(), array('ID' => $iblockId)) -> GetNext();
            
            if(is_array($iblockData) && count($iblockData) > 0) {
                $result = true;
            } else {
                $result = false;
            }
        } else {
            $result = false;
        }
        
        return $result;
    }
}
