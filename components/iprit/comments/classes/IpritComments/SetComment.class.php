<?php
namespace Bitrix\IpritComments;

use Bitrix\Main\Loader;

class SetComment
{
    /**
     * Подключаем модуль для работы с инфоблоками
     */
    public function __construct()
    {
        Loader::includeModule('iblock');
    }
    
    /**
     * 
     * @param int $iblockId int - id инфоблока
     * @param string $code string - код страницы/элемента
     * @param array $values - массив со значениями полей комментария: name, text, answer
     */
    public function save($iblockId, $code, $values)
    {
        $addItem = new \CIBlockElement;
        
        $addItem -> Add([
            'IBLOCK_ID' => $iblockId,
            'CODE' => $code,
            'SORT' => (int)$values['answer'],
            'NAME' => htmlspecialcharsEx($values['name']),
            'PREVIEW_TEXT' => htmlspecialcharsEx($values['text']),
            'ACTIVE' => 'Y'
        ]);
    }
}
