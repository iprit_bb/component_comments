<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}

if(!CModule::IncludeModule("iblock")) {
	return;
}

$context = \Bitrix\Main\Context::getCurrent();

$arIBlocks = [];

/**
 * TODO - заменить на аналог D7
 */
$db_iblock = \CIBlock::GetList(
    ['SORT' => 'ASC'], 
    [
        'SITE_ID' => $context->getSite(), 
        'TYPE' => ''
    ]
);

while($arRes = $db_iblock->Fetch()) {
	$arIBlocks[$arRes["ID"]] = $arRes["NAME"];
}

$arComponentParameters = [
    'GROUPS' => [],
    'PARAMETERS' => [
        'IBLOCK_ID' => [
			'PARENT' => 'BASE',
			'NAME' => GetMessage('IBLOCK_ID_NAME'),
			'TYPE' => 'LIST',
			'VALUES' => $arIBlocks,
			'DEFAULT' => ''
		],
        'TYPE' => [
            'PARENT' => 'BASE',
            'NAME' => GetMessage('TYPE_NAME'),
            'TYPE' => 'LIST',
            'VALUES' => [
                'ELEMENT' => GetMessage('TYPE_ELEMENT'),
                'PAGE' => GetMessage('TYPE_PAGE')
            ],
            'DEFAULT' => 'ELEMENT'
        ],
        'CODE' => [
            'PARENT' => 'BASE',
            'NAME' => GetMessage('CODE_NAME'),
            'TYPE' => 'STRING',
            'DEFAULT' => ''
        ],
    ],
];